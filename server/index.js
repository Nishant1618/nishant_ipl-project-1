const fs =require("fs");
const csv = require("csvtojson");

const matchesPerYear = require("../public/output/matchesPerYear");
const matchesWonByEachTeam = require("../public/output/matchesWonByEachTeam");
const extraRunsConcededByEachTeam = require("../public/output/extraRunsConcededByEachTeam");
const topTenEconomicalBowlers = require("../public/output/topTenEconomicalBowlers");

const MATCHES_FILE_PATH = "../data/matches.csv";
const DELIVERIES_FILE_PATH = "../data/deliveries.csv";
const JSON_OUTPUT_FILE_PATH = "../public/data.json";

function main() {
  csv()
    .fromFile(MATCHES_FILE_PATH)
    .then(matches => {
     csv()
    .fromFile(DELIVERIES_FILE_PATH)
    .then(deliveries => {
      let result1 = matchesPerYear(matches);
      let result2 = matchesWonByEachTeam(matches);
      
      let result3 = extraRunsConcededByEachTeam(matches,deliveries);
      let result4 = topTenEconomicalBowlers(matches,deliveries);
     
      saveMatchesPlayedPerYear(result1,result2,result3,result4);
    });
  });
}


 
function saveMatchesPlayedPerYear(result1,result2,result3,result4) {
  const jsonData = {
    matchesPerYear: result1,
    matchesWonByEachTeam: result2,
    extraRunsConcededByEachTeam:result3,
    topTenEconomicalBowlers :result4,
 
  };
  const jsonString = JSON.stringify(jsonData);
  fs.writeFile(JSON_OUTPUT_FILE_PATH, jsonString, "utf8", err => {
    if (err) {
      console.error(err);
    }
  });
}

main();